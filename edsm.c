/*
 * Final state machine lib.
 * Copyright (C) 2015 Ubiquiti Networks (yuchi.chen@ubnt.com)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
 
#include "edsm.h"

void EDSM_Init(EDSMSys *sys, int firstState)
{
   sys->state = firstState;
   sys->idx_S = 0;
   sys->idx_E = 0;
   //memset(sys->drivers, 0, sizeof(sys->drivers));
   //memset(sys->packages, 0, sizeof(sys->packages));
   pthread_spin_init(&(sys->lock), PTHREAD_PROCESS_SHARED);
}

void EDSM_NextRun(EDSMSys *sys)
{
   while (sys->idx_S != sys->idx_E)
   {
      pthread_spin_lock(&(sys->lock));
      EDSMEventDriver driver = (EDSMEventDriver)sys->drivers[sys->idx_S];
      void *package = sys->packages[sys->idx_S];
      sys->idx_S = (sys->idx_S+1) % MAX_OF_EVENT_DRIVERs;
      pthread_spin_unlock(&(sys->lock));

      driver(sys, package);
   }
}

void EDSM_AppendEventDriver(EDSMSys *sys, EDSMEventDriver driver, void *package)
{
   pthread_spin_lock(&(sys->lock));
   sys->drivers[sys->idx_E] = (void*)driver;
   sys->packages[sys->idx_E] = package;
   sys->idx_E = (sys->idx_E+1) % MAX_OF_EVENT_DRIVERs;
   pthread_spin_unlock(&(sys->lock));
}

int EDSM_State(EDSMSys *sys)
{
   return sys->state;
}

