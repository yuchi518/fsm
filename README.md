# State Machine

There are many kind of implementations about state machine. I will try to write them down in C code and share with everyone. If you have any idea, welcome to tell me.

Number of implementations I could think, as follwoing:

  - Switch Case State Machine (not implemented yet)
  - Function Table State Machine (not implemented yet)
  - Controllable Function Table State Machine (ex. cftsm.h, cftsm.c)
  - Event Driven State Machine (ex. edsm.h, edsm.c)
  - Macro defined State Machine (not implemented yet)

## Version
0.1

## Tech
(on going)

## TO-DO
 - Write Samples
 - Implement more state machines

## License
GPLv2


