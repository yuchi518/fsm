/*
 * Final state machine lib.
 * Copyright (C) 2015 Ubiquiti Networks (yuchi.chen@ubnt.com)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/// Controllable Function Table State Machine

#ifndef _FSM_H_
#define _FSM_H_

#include <pthread.h>

/**
 * If preState is from other state, it means state transition,
 * else it was idle in previous step.
 */

typedef int FSMState;
typedef FSMState (*FSMFunction)(FSMState preState, void *pars);
#define FSMState_IDLE 	((FSMState)-1)
#define FSMState_RESET 	((FSMState)-2)

typedef int FSMAction;
#define FSMAction_STOP					(1<<0)
#define FSMAction_RUN					(1<<1)
#define FSMAction_STOP_THEN_RUN		(FSMAction_STOP | FSMAction_RUN)

typedef struct {
   FSMFunction *fsmTable;
   FSMFunction reset;
   FSMState preState;
   FSMState state;
   FSMAction action;
   void *pars;
   pthread_spinlock_t lock;
} FSMStructure;

/******************************************************************************
 Declare functions
 ******************************************************************************/
 
void FSM_Init(FSMStructure *fsmstr, FSMFunction fsmTable[], FSMFunction reset, void *pars);
FSMState FSM_Run(FSMStructure *fsmstr);				// call this to process next step
void FSM_Restart(FSMStructure *fsmstr);		// reset in next step and then run
void FSM_Stop(FSMStructure *fsmstr);			// reset in next step and then stop
void FSM_Start(FSMStructure *fsmstr);			// run in next step
FSMState FSM_State(FSMStructure *fsmstr);
FSMState FSM_PreState(FSMStructure *fsmstr);


#endif


