/*
 * Final state machine lib.
 * Copyright (C) 2015 Ubiquiti Networks (yuchi.chen@ubnt.com)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "cftsm.h"

void FSM_Init(FSMStructure *fsmstr, FSMFunction fsmTable[], FSMFunction reset, void *pars)
{
   fsmstr->fsmTable = fsmTable;
   fsmstr->reset = reset;
   fsmstr->preState = FSMState_IDLE;
   fsmstr->state = FSMState_RESET;
   fsmstr->action = FSMAction_STOP; //_THEN_RUN;
   fsmstr->pars = pars;

   pthread_spin_init(&(fsmstr->lock), PTHREAD_PROCESS_SHARED);
}

FSMState FSM_Run(FSMStructure *fsmstr)
{
   FSMState ns = FSMState_IDLE;

   pthread_spin_lock(&(fsmstr->lock));

   if (fsmstr->action & FSMAction_STOP)
   {
      ns = fsmstr->reset(fsmstr->preState, fsmstr->pars);
      fsmstr->action &= ~FSMAction_STOP;
   }
   else if (fsmstr->action & FSMAction_RUN)
   {
      ns = fsmstr->fsmTable[fsmstr->state](fsmstr->preState, fsmstr->pars);
   }

   fsmstr->preState = fsmstr->state;
   
   if (ns >= 0) fsmstr->state = ns;
   else {
      switch (ns) {
      case FSMState_IDLE:
         break;
      case FSMState_RESET:
         FSM_Restart(fsmstr);
         break;
      }
   }

   pthread_spin_unlock(&(fsmstr->lock));

   return ns;
}

void FSM_Restart(FSMStructure *fsmstr)
{
   pthread_spin_lock(&(fsmstr->lock));
   fsmstr->action |= FSMAction_STOP_THEN_RUN;
   pthread_spin_unlock(&(fsmstr->lock));
}

void FSM_Stop(FSMStructure *fsmstr)
{
   pthread_spin_lock(&(fsmstr->lock));
   fsmstr->action = FSMAction_STOP;
   pthread_spin_unlock(&(fsmstr->lock));
}

void FSM_Start(FSMStructure *fsmstr)
{
   pthread_spin_lock(&(fsmstr->lock));
   fsmstr->action |= FSMAction_RUN;
   pthread_spin_unlock(&(fsmstr->lock));
}

FSMState FSM_State(FSMStructure *fsmstr)
{
   FSMState state;
   pthread_spin_lock(&(fsmstr->lock));
   state = fsmstr->state;
   pthread_spin_unlock(&(fsmstr->lock));
   return state;
}

FSMState FSM_PreState(FSMStructure *fsmstr)
{
   FSMState state;
   pthread_spin_lock(&(fsmstr->lock));
   state = fsmstr->preState;
   pthread_spin_unlock(&(fsmstr->lock));
   return state;
}


