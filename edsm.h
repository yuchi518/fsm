/*
 * Final state machine lib.
 * Copyright (C) 2015 Ubiquiti Networks (yuchi.chen@ubnt.com)
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/// Event Drivent State Machine

#ifndef _EDSM_H_
#define _EDSM_H_

#include <pthread.h>

#define MAX_OF_EVENT_DRIVERs			16

typedef struct {
	int state;
	int idx_S, idx_E;
	void *drivers[MAX_OF_EVENT_DRIVERs];			// EDSMEventDriver
	void *packages[MAX_OF_EVENT_DRIVERs];
	pthread_spinlock_t lock;
} EDSMSys;

typedef void (*EDSMEventDriver)(EDSMSys *sys, void *package);

void EDSM_Init(EDSMSys *sys, int firstState);
void EDSM_NextRun(EDSMSys *sys);
void EDSM_AppendEventDriver(EDSMSys *sys, EDSMEventDriver driver, void *package);

int EDSM_State(EDSMSys *sys);


#endif

